const gulp = require("gulp");
const gulpSass = require("gulp-sass");
const browserSync = require("browser-sync");
const gulpAutoPrefixer = require("gulp-autoprefixer");
const gulpConcat = require("gulp-concat");
const gulpMinifycss = require("gulp-clean-css");
const gulpImgmin = require("gulp-imagemin");
const cleany = require("gulp-clean");
const uglify = require("gulp-uglify");
const { parallel, series } = require("gulp");
const { src, dest } = require("gulp");
const { watch } = require("gulp");

//Build part

function clean() {
  return gulp.src("dist/*", { read: false }).pipe(cleany());
}

function html() {
  return gulp
    .src("index.html")
    .pipe(gulp.dest("dist"))
    .pipe(browserSync.reload({ stream: true }));
}

function scss() {
  return gulp
    .src("src/styles/**/*.scss")
    .pipe(gulpSass())
    .pipe(gulp.dest("dist/css"))
    .pipe(browserSync.reload({ stream: true }));
}

function modify() {
  return gulp
    .src("src/styles/**/*.scss")
    .pipe(gulpSass())
    .pipe(
      gulpAutoPrefixer(["last 15 versions", "> 1%", "ie 8", "ie 7"], {
        cascade: true,
      })
    )
    .pipe(gulpConcat("styles.min.css"))
    .pipe(gulpMinifycss({ compatibility: "ie8" }))
    .pipe(gulp.dest("dist/css"))
    .pipe(browserSync.reload({ stream: true }));
}

function js() {
  return gulp
    .src("src/js/*.js")
    .pipe(gulpConcat("scripts.min.js"))
    .pipe(uglify())
    .pipe(gulp.dest("dist/js"))
    .pipe(browserSync.reload({ stream: true }));
}

function img() {
  return gulp
    .src("src/images/**/*")
    .pipe(gulpImgmin())
    .pipe(gulp.dest("dist/img"))
    .on("end", browserSync.reload);
}

function copy() {
  return src("src/styles/reset.css").pipe(dest("dist/css"));
}
// Dev part
function serv() {
  browserSync.init({
    server: {
      baseDir: "./dist",
    },
  });
}

function bsReload() {
  return browserSync.reload();
}

function watcher() {
  watch("./dist/index.html").on("change", bsReload);
  watch("*.html").on("change", parallel(html, bsReload));
  watch("./src/js/*.js").on("change", parallel(js));
  watch("./src/styles/*.scss").on("change", series(modify));

  watch("./src/images/**/*.{jpg,jpeg,png,gif,tiff,svg}").on(
    "change",
    parallel(img)
  );
}

exports.build = series(clean, parallel(html, modify, js, img, copy));
exports.dev = parallel(serv, watcher);
