let menuItem = document.querySelectorAll(".menu__item");
let openBurger = document.querySelector(".fa-bars");
let closeBurger = document.querySelector(".fa-times-circle");
let burger = document.querySelectorAll(".header__burger");
let headerMenu = document.querySelector(".header__menu");
let headerNav = document.querySelector(".header__nav");
closeBurger.style.display = "none";
function menuToggle(event) {
  if (event.target === openBurger) {
    closeBurger.style.display = "block";
    openBurger.style.display = "none";
    menuItem[1].style.display = "block";
    headerMenu.style.position = "absolute";
    headerMenu.style.top = "0";
    headerMenu.style.right = "0";
    headerMenu.style.paddingTop = "41px";
    headerNav.style.paddingBottom = "48px";

    for (let i = 2; i < menuItem.length; i++) {
      menuItem[i].style.display = "block";
      menuItem[i].style.position = "none";
    }
  }
  if (event.target === closeBurger) {
    closeBurger.style.display = "none";
    openBurger.style.display = "block";
    for (let i = 2; i < menuItem.length; i++) {
      menuItem[i].style.display = "none";
    }
  }
}
openBurger.addEventListener("click", menuToggle);
closeBurger.addEventListener("click", menuToggle);
