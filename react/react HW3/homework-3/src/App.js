import "./reset.css";
import "./App.scss";
import { Route, Switch } from "react-router";
import React, { useEffect, useState } from "react";
import { getProducts } from "./API/products";
import Header from "./Components/Header/Header";
import Cart from "./Pages/Cart.jsx";
import Favourites from "./Pages/Favourites";
import PhonesList from "./Pages/PhonesList";
const getCartFromLS = () => {
  const cart = localStorage.getItem("cart");
  if (!cart) return [];
  const parsedCart = JSON.parse(cart);
  return parsedCart;
};
const getFavouritesFromLS = () => {
  const favourites = localStorage.getItem("favourites");
  if (!favourites) return [];
  const parsedfavourites = JSON.parse(favourites);
  return parsedfavourites;
};
const App = () => {
  const [products, setProducts] = useState([]);
  const [cart, setCart] = useState(getCartFromLS());
  const [favourites, setFavourites] = useState(getFavouritesFromLS());

  const addToCart = (product) => {
    if (cart.some((e) => e.name === product.name)) {
      return null;
    } else {
      setCart([...cart, product]);
    }
  };
  const handleRemoveFromCart = (product) => {
    const filteredCart = cart.filter((e) => e.name !== product.name);
    setCart(filteredCart);
  };
  const addFavourite = (product) => {
    if (favourites.some((e) => e.name === product.name)) {
      return null;
    } else {
      setFavourites([...favourites, product]);
    }
  };
  const handleRemoveFavourites = (product) => {
    const filteredFavourites = favourites.filter(
      (e) => e.name !== product.name
    );
    setFavourites(filteredFavourites);
  };

  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cart));
  }, [cart]);
  useEffect(() => {
    localStorage.setItem("favourites", JSON.stringify(favourites));
  }, [favourites]);
  const fetchProducts = () => {
    getProducts().then((productsData) => {
      setProducts(productsData);
    });
  };
  useEffect(() => {
    fetchProducts();
  }, []);

  return (
    <>
      <Header />

      <main>
        <Switch>
          <Route path="/phonesList">
            <PhonesList
              addToCart={addToCart}
              addFavourite={addFavourite}
              products={products}
              handleRemoveFavourites={handleRemoveFavourites}
            ></PhonesList>
          </Route>
          <Route exact path="/cart">
            <Cart
              addToCart={addToCart}
              addFavourite={addFavourite}
              products={cart}
              handleRemoveFromCart={handleRemoveFromCart}
              handleRemoveFavourites={handleRemoveFavourites}
            ></Cart>
          </Route>
          <Route exact path="/favourites">
            <Favourites
              addToCart={addToCart}
              addFavourite={addFavourite}
              products={favourites}
              handleRemoveFavourites={handleRemoveFavourites}
            ></Favourites>
          </Route>
        </Switch>
      </main>
    </>
  );
};

export default App;
