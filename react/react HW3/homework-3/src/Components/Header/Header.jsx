import { Link } from "react-router-dom";
import "./header.scss";
const Header = () => {
  return (
    <>
      <header>
        <h1 className="header">SmartphoneXpress</h1>
        <nav>
          <ul className="header-list">
            <li>
              <Link className="header-item" to="/phonesList">
                Phones
              </Link>
            </li>
            <li>
              <Link className="header-item" to="/cart">
                Cart
              </Link>
            </li>
            <li>
              <Link className="header-item" to="/favourites">
                Favourites
              </Link>
            </li>
          </ul>
        </nav>
      </header>
    </>
  );
};

export default Header;
