import React from "react";
import "./button.scss";
import PropTypes from "prop-types";
const Button = ({ text, styleClass, handleClick }) => {
  return (
    <button className={[styleClass, "button"].join(" ")} onClick={handleClick}>
      {text}
    </button>
  );
};
Button.propTypes = {
  text: PropTypes.string,
  styleClass: PropTypes.string,
  openModal: PropTypes.func,
};
export default Button;
