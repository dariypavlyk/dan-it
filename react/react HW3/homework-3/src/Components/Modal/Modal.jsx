import React from "react";
import "./modal.scss";
import "../Product/Product.scss";
import PropTypes from "prop-types";
const Modal = ({ show, onClose, header, closeButton, text, actions }) => {
  const modalContent = (
    <div className=" modal-dialog">
      <div className="modal-content ">
        <div className="modal-header">
          <h4 className="modal-title">{header}</h4>
          {closeButton ? (
            <span onClick={onClose} className="close-button"></span>
          ) : null}
        </div>
        {text}

        {actions}
      </div>
    </div>
  );
  return !show ? null : modalContent;
};
Modal.propTypes = {
  show: PropTypes.bool,
  onClose: PropTypes.func,
  header: PropTypes.string,
  closeButton: PropTypes.bool,
  text: PropTypes.object,
  actions: PropTypes.object,
};
export default Modal;
