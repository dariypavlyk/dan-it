import ProductsList from "../Components/ProductsList/ProductsList.jsx";
const Cart = ({
  addToCart,
  products,
  addFavourite,
  handleRemoveFromCart,
  handleRemoveFavourites,
}) => {
  return (
    <ProductsList
      addToCart={addToCart}
      addFavourite={addFavourite}
      products={products}
      header="Cart"
      removeFromCart={true}
      handleRemoveFromCart={handleRemoveFromCart}
      handleRemoveFavourites={handleRemoveFavourites}
    />
  );
};

export default Cart;
