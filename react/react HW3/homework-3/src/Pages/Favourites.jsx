import ProductsList from "../Components/ProductsList/ProductsList.jsx";
import React, { useEffect, useState } from "react";
const Favourites = ({
  addToCart,
  products,
  addFavourite,
  handleRemoveFavourites,
}) => {
  return (
    <ProductsList
      addToCart={addToCart}
      addFavourite={addFavourite}
      products={products}
      header="Favourites"
      removeFromCart={false}
      handleRemoveFavourites={handleRemoveFavourites}
      initialFavouriteDisplay={true}
    />
  );
};

export default Favourites;
