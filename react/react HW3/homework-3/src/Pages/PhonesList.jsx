import ProductsList from "../Components/ProductsList/ProductsList.jsx";
import React, { useEffect, useState } from "react";
const PhonesList = ({
  addToCart,
  products,
  addFavourite,
  handleRemoveFavourites,
}) => {
  return (
    <ProductsList
      addToCart={addToCart}
      addFavourite={addFavourite}
      products={products}
      header="Phones"
      removeFromCart={false}
      handleRemoveFavourites={handleRemoveFavourites}
    />
  );
};

export default PhonesList;
