export const favouritesReducer = (state = [], action) => {
  switch (action.type) {
    case "ADD_TO_FAVOURITES":
      localStorage.setItem(
        "favourites",
        JSON.stringify([...state, action.payload])
      );
      return [...state, action.payload];
    case "REVOME_FROM_FAVOURITES":
      const filteredFavourites = state.filter(
        (e) => e.name !== action.payload.name
      );
      localStorage.setItem("favourites", JSON.stringify(filteredFavourites));
      return filteredFavourites;
    case "SET_FAVOURITES":
      const favourites = localStorage.getItem("favourites");
      if (!favourites) return [];
      const parsedfavourites = JSON.parse(favourites);
      return parsedfavourites;
    default:
      return state;
  }
};
