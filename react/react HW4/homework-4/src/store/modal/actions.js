export const showModal = (data) => {
  return {
    type: "OPEN_MODAL",
    payload: data,
  };
};

export const closeModal = (data) => {
  return {
    type: "CLOSE_MODAL",
    payload: data,
  };
};
