import { getProducts } from "../../API/products";

const setProducts = (products) => {
  return {
    type: "SET_PRODUCTS",
    payload: products,
  };
};

export const fetchProducts = () => (dispatch, getState) => {
  getProducts().then((products) => {
    dispatch(setProducts(products));
  });
};
