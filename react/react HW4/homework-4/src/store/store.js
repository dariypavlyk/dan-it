import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import { productsReducer } from "./products/reducer";
import { cartReducer } from "./cart/reducer";
import { favouritesReducer } from "./favourites/reducer";
import thunk from "redux-thunk";
import { modalReducer } from "./modal/reducer";
const rootReducer = combineReducers({
  products: productsReducer,
  cart: cartReducer,
  favourites: favouritesReducer,
  modal: modalReducer,
});
const devTools = window.__REDUX_DEVTOOLS_EXTENSION__
  ? window.__REDUX_DEVTOOLS_EXTENSION__()
  : (args) => args;

const store = createStore(
  rootReducer,
  compose(applyMiddleware(thunk), devTools)
);
export default store;
