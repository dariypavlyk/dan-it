import ProductsList from "../Components/ProductsList/ProductsList.jsx";

import { useSelector } from "react-redux";
const PhonesList = () => {
  const products = useSelector(({ products }) => products);
  return <ProductsList products={products} header="Phones" />;
};

export default PhonesList;
