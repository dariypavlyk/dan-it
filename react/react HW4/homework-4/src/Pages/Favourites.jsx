import ProductsList from "../Components/ProductsList/ProductsList.jsx";

import { useSelector } from "react-redux";
const Favourites = () => {
  const favourites = useSelector(({ favourites }) => favourites);
  return <ProductsList products={favourites} header="Favourites" />;
};

export default Favourites;
