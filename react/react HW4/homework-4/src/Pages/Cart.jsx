import ProductsList from "../Components/ProductsList/ProductsList.jsx";
import { useSelector } from "react-redux";

const Cart = () => {
  const cart = useSelector(({ cart }) => cart);
  return <ProductsList products={cart} header="Cart" />;
};

export default Cart;
