export const getProducts = () => {
  return fetch("/products.json").then((response) => {
    if (response.ok) {
      return response.json();
    }
    return Promise.reject(response);
  });
};
