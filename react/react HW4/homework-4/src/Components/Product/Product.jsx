import "./Product.scss";
import { showModal } from "../../store/modal/actions";
import Button from "../Button/Button.jsx";

import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";

import {
  addToFavourites,
  removeFromFavourites,
} from "../../store/favourites/actions";

const Product = ({ product }) => {
  const dispatch = useDispatch();
  //Cart section

  const cartItems = useSelector(({ cart }) => cart);
  const isInCart = cartItems.some((cartItem) => cartItem.id === product.id);
  //Favourites section
  const addToFavouritesHandler = () => {
    dispatch(addToFavourites(product));
  };
  const removeFromFavouritesHandler = () => {
    dispatch(removeFromFavourites(product));
  };
  const favouriteItems = useSelector(({ favourites }) => favourites);
  const isFavourite = favouriteItems.some(
    (favouriteItem) => favouriteItem.id === product.id
  );

  //Modal display
  const showModalHandle = () => {
    dispatch(showModal(product));
  };

  //Modal section
  // const [modalDisplay, setModalDisplay] = useState(false);
  // const showModal = (e) => {
  //   document.addEventListener("click", handleOutsideClick);
  //   setModalDisplay(true);
  // };
  // const closeModal = (e) => {
  //   setModalDisplay(false);
  // };
  // const handleOutsideClick = (e) => {
  //   if (
  //     !e.target.closest(".modal-content") &&
  //     !e.target.classList.contains("action-buy")
  //   ) {
  //     closeModal();
  //   }
  // };
  // Modal content section
  // const modalActions = (
  //   <div className="modal-footer">
  //     <Button
  //       styleClass="action-buy"
  //       text="Confirm"
  //       handleClick={
  //         isInCart
  //           ? () => {
  //               removeFromCartHandler();
  //               closeModal();
  //             }
  //           : () => {
  //               addToCartHandler();
  //               closeModal();
  //             }
  //       }
  //     ></Button>
  //     <Button
  //       styleClass="action-buy"
  //       text="Cancel"
  //       handleClick={closeModal}
  //     ></Button>
  //   </div>
  // );

  return (
    <>
      <div className="phone-image-container">
        <img src={product.url} alt="" className="phone-image" />
      </div>

      <h3 className="phone-name">{product.name}</h3>
      <div className="phone-details-container">
        <p>Color: {product.color}</p>

        <p>Product ID: {product.id}</p>
      </div>

      <p className="phone-price">{product.price} UAH</p>
      <div className="actions-container">
        {isInCart ? (
          <Button
            styleClass="action-buy"
            text="Remove from cart"
            handleClick={showModalHandle}
          ></Button>
        ) : (
          <Button
            styleClass="action-buy"
            text="Add to cart"
            handleClick={showModalHandle}
          ></Button>
        )}

        <svg
          onClick={
            isFavourite
              ? () => {
                  removeFromFavouritesHandler();
                }
              : () => {
                  addToFavouritesHandler();
                }
          }
          width="16"
          height="16"
          fill={isFavourite ? "#da2533" : "#797979"}
          className="phone-favicon"
        >
          <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
        </svg>
      </div>
    </>
  );
};
Product.propTypes = {
  product: PropTypes.object,
};
export default Product;
