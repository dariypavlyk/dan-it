import React from "react";
import "./modal.scss";
import "../Product/Product.scss";
import { closeModal } from "../../store/modal/actions";
import { useDispatch, useSelector } from "react-redux";
import Button from "../Button/Button.jsx";
import { addToCart, removeFromCart } from "../../store/cart/actions";
const Modal = () => {
  const dispatch = useDispatch();
  const modal = useSelector((state) => state.modal);
  const cartItems = useSelector(({ cart }) => cart);
  const isInCart = cartItems.some(
    (cartItem) => cartItem.id === modal.product.id
  );
  const product = modal.product;
  const modalDisplay = modal.modalDisplay;
  const header = modal.product.name;
  const addToCartHandler = () => {
    dispatch(addToCart(product));
  };
  const removeFromCartHandler = () => {
    dispatch(removeFromCart(product));
    console.log(product);
  };
  const modalContent = (
    <div className=" modal-dialog">
      <div className="modal-content ">
        <div className="modal-header">
          <h4 className="modal-title">{header}</h4>

          <span
            onClick={() => {
              dispatch(closeModal({}));
            }}
            className="close-button"
          ></span>
        </div>
        <p className="modal-body">
          {isInCart
            ? "Would you like to remove this phone from your cart?"
            : "Would you like to add this phone to your cart?"}
        </p>
        <div className="modal-footer">
          <Button
            styleClass="action-buy"
            text="Confirm"
            handleClick={
              isInCart
                ? () => {
                    removeFromCartHandler();
                    dispatch(closeModal({}));
                  }
                : () => {
                    addToCartHandler();
                    dispatch(closeModal({}));
                  }
            }
          ></Button>
          <Button
            styleClass="action-buy"
            text="Cancel"
            handleClick={closeModal}
          ></Button>
        </div>
      </div>
    </div>
  );
  return modalDisplay ? modalContent : null;
};

export default Modal;
