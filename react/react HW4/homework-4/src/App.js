import "./reset.css";
import "./App.scss";
import { Route, Switch } from "react-router";
import React, { useEffect } from "react";
import Modal from "./Components/Modal/Modal";
import Header from "./Components/Header/Header";
import Cart from "./Pages/Cart.jsx";
import Favourites from "./Pages/Favourites";
import PhonesList from "./Pages/PhonesList";
import { fetchProducts } from "./store/products/actions";
import { setCart } from "./store/cart/actions";
import { setFavourites } from "./store/favourites/actions";
import { useDispatch } from "react-redux";

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchProducts());
    dispatch(setCart());
    dispatch(setFavourites());
  });

  return (
    <>
      <Header />

      <main>
        <Switch>
          <Route path="/phonesList">
            <PhonesList></PhonesList>
          </Route>
          <Route exact path="/cart">
            <Cart></Cart>
          </Route>
          <Route exact path="/favourites">
            <Favourites></Favourites>
          </Route>
        </Switch>
      </main>
      <Modal />
    </>
  );
};

export default App;
