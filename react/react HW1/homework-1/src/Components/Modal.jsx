import React from "react";
import "./modal.scss";
class Modal extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (!this.props.show) {
      return null;
    }

    return (
      <div className="show modal-customized">
        <div className="modal-dialog ">
          <div className="modal-content ">
            <div className="modal-header">
              <h4 className="modal-title">{this.props.header}</h4>
              {this.props.closeButton ? (
                <button
                  onClick={this.props.onClose}
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              ) : null}
            </div>

            {this.props.text}

            {this.props.actions}
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
