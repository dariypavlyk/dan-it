import React from "react";

import "./button.scss";
class Button extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <>
        <button
          className={[this.props.backgroundColor, "button"].join(" ")}
          onClick={this.props.openModal}
        >
          {this.props.text}
        </button>
      </>
    );
  }
}

export default Button;
