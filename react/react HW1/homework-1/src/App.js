import "./App.scss";
import React from "react";
import Button from "./Components/Button.jsx";
import Modal from "./Components/Modal.jsx";
class App extends React.Component {
  constructor() {
    super();
    this.state = { firstModalDisplay: false, secondModalDisplay: false };
  }

  showFirstModal = (e) => {
    if (!this.state.showFirstModal) {
      document.addEventListener("click", this.handleOutsideClick);
    } else {
      document.removeEventListener("click", this.handleOutsideClick);
    }
    if (this.state.secondModalDisplay === true) {
      this.setState({
        secondModalDisplay: false,
      });
    }
    this.setState({
      firstModalDisplay: true,
    });
  };
  showSecondModal = (e) => {
    if (this.state.firstModalDisplay === true) {
      this.setState({
        firstModalDisplay: false,
      });
    }
    this.setState({
      secondModalDisplay: true,
    });
  };
  closeModal = (e) => {
    console.log("hi from close");
    this.setState({
      firstModalDisplay: false,
      secondModalDisplay: false,
    });
  };
  handleOutsideClick = (e) => {
    if (
      !e.target.closest(".modal-content") &&
      !e.target.className.includes("button")
    ) {
      this.closeModal();
    }
  };
  render() {
    const firstModalActions = (
      <div className="modal-footer">
        <button className="btn btn-primary">Ok</button>
        <button className="btn btn-secondary">Cancel</button>
      </div>
    );
    const secondModalActions = (
      <div className="modal-footer">
        <button className="btn btn-outline-primary">Subscribe</button>
        <button className="btn btn-outline-secondary">Delete</button>
      </div>
    );
    const firstModalText = (
      <p className="modal-body">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum
        elementum mi nulla.
      </p>
    );
    const secondModalText = (
      <p className="modal-body">
        Proin lacus nisl, dictum quis metus at, pulvinar feugiat purus. Maecenas
        pulvinar.
      </p>
    );
    return (
      <>
        <div className="button-container">
          <Button
            text="Open first modal"
            backgroundColor="button-red"
            openModal={this.showFirstModal}
          />
          <Button
            text="Open second modal"
            backgroundColor="button-blue"
            openModal={this.showSecondModal}
          />
        </div>

        <Modal
          show={this.state.firstModalDisplay}
          onClose={this.closeModal}
          header={"First modal header"}
          closeButton={true}
          text={firstModalText}
          actions={firstModalActions}
        />

        <Modal
          show={this.state.secondModalDisplay}
          onClose={this.closeModal}
          header={"Second modal header"}
          closeButton={false}
          text={secondModalText}
          actions={secondModalActions}
        />
      </>
    );
  }
}

export default App;
