import ProductsList from "../Components/ProductsList/ProductsList.jsx";
import { useSelector } from "react-redux";
import CheckoutForm from "../Components/CheckoutForm/CheckoutForm";
const Cart = () => {
  const cart = useSelector(({ cart }) => cart);
  return (
    <>
      <ProductsList products={cart} />
      <CheckoutForm />
    </>
  );
};

export default Cart;
