import React, { useState } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import "./CheckoutForm.scss";
import { cleanCart } from "../../store/cart/actions";
import { useDispatch, useSelector } from "react-redux";
import CheckoutDetails from "../CheckoutDetails/CheckoutDetails";
const FormSchema = Yup.object({
  name: Yup.string().required("Name is required"),
  surname: Yup.string().required("Surname is required"),
  age: Yup.number()
    .typeError("Please enter a number")
    .required("Age is required")
    .positive()
    .integer(),
  address: Yup.string()
    .min(10, "Please enter the full address")
    .required("Delivery address is required"),
  phoneNumber: Yup.number()
    .typeError("Please enter a number")
    .required("Phone number is required")
    .positive()
    .integer(),
});

export default function CheckoutForm() {
  const dispatch = useDispatch();
  const [checkoutCart, setcheckoutCart] = useState([]);
  const [userInfo, setuserInfo] = useState({});
  const [isCheckout, setisCheckout] = useState(false);
  const cartItems = useSelector(({ cart }) => cart);

  const handleFormSubmit = (values, { resetForm }) => {
    const { name, surname, age, address, phoneNumber } = values;
    const actualUserInfo = {
      name: name,
      surname: surname,
      age: age,
      address: address,
      phoneNumber: phoneNumber,
    };
    setuserInfo(actualUserInfo);
    setcheckoutCart(cartItems);
    setisCheckout(true);
    resetForm();
    dispatch(cleanCart());
  };
  return (
    <>
      {!isCheckout && (
        <>
          {" "}
          <h2 className="checkout-header">You can proceed to checkout here</h2>
          <Formik
            initialValues={{
              name: "",
              surname: "",
              age: "",
              address: "",
              phoneNumber: "",
            }}
            validationSchema={FormSchema}
            onSubmit={handleFormSubmit}
          >
            {({ isSubmitting }) => (
              <Form className="checkout-form">
                <div className="form-container">
                  <div className="form-list-item">
                    <label htmlFor="name">First Name</label>
                    <Field
                      className="form-field"
                      name="name"
                      type="name"
                      placeholder="Name"
                    />
                    <ErrorMessage
                      component="div"
                      className="error-message"
                      name="name"
                    />
                  </div>
                  <div className="form-list-item">
                    <label htmlFor="name">Surname</label>
                    <Field
                      name="surname"
                      type="surname"
                      placeholder="Surname"
                      className="form-field"
                    />
                    <ErrorMessage
                      className="error-message"
                      component="div"
                      name="surname"
                    />
                  </div>
                  <div className="form-list-item">
                    <label htmlFor="name">Age</label>
                    <Field
                      name="age"
                      type="age"
                      placeholder="Age"
                      className="form-field"
                    />
                    <ErrorMessage
                      className="error-message"
                      component="div"
                      name="age"
                    />
                  </div>
                  <div className="form-list-item">
                    <label htmlFor="name">Delivery address</label>
                    <Field
                      name="address"
                      type="address"
                      placeholder="Delivery address"
                      className="form-field"
                    />
                    <ErrorMessage
                      className="error-message"
                      component="div"
                      name="address"
                    />
                  </div>
                  <div className="form-list-item">
                    <label htmlFor="name">Phone number</label>
                    <Field
                      name="phoneNumber"
                      type="phoneNumber"
                      placeholder="Phone number"
                      className="form-field"
                    />
                    <ErrorMessage
                      className="error-message"
                      component="div"
                      name="phoneNumber"
                    />
                  </div>
                  <button className="button" type="submit">
                    Checkout
                  </button>
                </div>
              </Form>
            )}
          </Formik>{" "}
        </>
      )}
      {isCheckout && (
        <>
          {" "}
          <CheckoutDetails checkoutCart={checkoutCart} userInfo={userInfo} />
        </>
      )}
    </>
  );
}
