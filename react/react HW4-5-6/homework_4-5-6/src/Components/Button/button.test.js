import Button from "./Button";
import { render, screen, fireEvent } from "@testing-library/react";

describe("<Button /> component", () => {
  it("should render", () => {
    const { getByText } = render(<Button text="Add to cart" />);
    getByText("Add to cart");
  });
  it("should contain button class", () => {
    render(<Button category="Add to cart" />);
    const el = screen.getByTestId("button");
    expect(el.classList).toContain("button");
  });
  it("should contain additional class when props are passed", () => {
    render(<Button category="Add to cart" styleClass="trial" />);
    const el = screen.getByTestId("button");
    expect(el.classList).toContain("trial");
  });
  it("should use the function from props on click", () => {
    const onClick = jest.fn();
    render(<Button category="Add to cart" handleClick={onClick} />);
    const el = screen.getByTestId("button");
    fireEvent.click(el);
    expect(onClick).toHaveBeenCalled();
  });
});
