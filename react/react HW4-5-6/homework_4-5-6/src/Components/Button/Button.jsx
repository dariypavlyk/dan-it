import React from "react";
import "./button.scss";
import PropTypes from "prop-types";
const Button = ({ text, styleClass, handleClick }) => {
  const buttonStyle = [styleClass, "button"].join(" ");
  return (
    <button data-testid="button" className={buttonStyle} onClick={handleClick}>
      {text}
    </button>
  );
};
Button.propTypes = {
  text: PropTypes.string,
  styleClass: PropTypes.string,
  openModal: PropTypes.func,
};
export default Button;
