import Product from "../Product/Product";
import "./ProductsList.scss";
import PropTypes from "prop-types";

const ProductsList = ({ products }) => {
  return (
    <div className="products-container">
      <ul className="phones-list">
        {products.map((product) => {
          return (
            <li className="phones-item" key={product.id}>
              <Product product={product} />
            </li>
          );
        })}
      </ul>
    </div>
  );
};
ProductsList.propTypes = {
  products: PropTypes.array,
};
export default ProductsList;
