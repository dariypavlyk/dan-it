import Modal from "./Modal";
import { render, screen, fireEvent } from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { closeModal } from "../../store/modal/actions";
const mockStore = configureStore();

describe("<Modal /> component", () => {
  it("should render", () => {
    const store = mockStore({
      favourites: [],
      cart: [],
      modal: {
        product: {},
      },
    });
    render(
      <Provider store={store}>
        <Modal />
      </Provider>
    );
  });
  it("should render 'remove from cart' text when the item is in the Cart", () => {
    const store = mockStore({
      favourites: [],
      cart: [{ id: 4 }],
      modal: {
        modalDisplay: true,
        product: { id: 4 },
      },
    });
    render(
      <Provider store={store}>
        <Modal />
      </Provider>
    );
    const el = screen.getByTestId("body-test");
    expect(el.textContent).toContain(
      "Would you like to remove this phone from your cart?"
    );
  });
  it("should render 'add to cart' text when the item is not in the Cart", () => {
    const store = mockStore({
      favourites: [],
      cart: [{ id: 4 }],
      modal: {
        modalDisplay: true,
        product: { id: 1 },
      },
    });
    render(
      <Provider store={store}>
        <Modal />
      </Provider>
    );
    const el = screen.getByTestId("body-test");
    expect(el.textContent).toContain(
      "Would you like to add this phone to your cart?"
    );
  });
  it("should dispatch close modal", () => {
    const store = mockStore({
      favourites: [],
      cart: [{ id: 4 }],
      modal: {
        modalDisplay: true,
        product: { id: 1 },
      },
    });
    store.dispatch(closeModal({}));
    const actions = store.getActions();
    expect(actions).toEqual([{ type: "CLOSE_MODAL", payload: {} }]);
  });
  it("should dispatch close modal action when clicking on span", () => {
    const store = mockStore({
      favourites: [],
      cart: [{ id: 4 }],
      modal: {
        modalDisplay: true,
        product: { id: 1 },
      },
    });
    render(
      <Provider store={store}>
        <Modal />
      </Provider>
    );
    const spanEl = screen.getByTestId("span-close-button");
    fireEvent.click(spanEl);
    const actions = store.getActions();

    expect(actions).toEqual([{ type: "CLOSE_MODAL", payload: {} }]);
  });
});
