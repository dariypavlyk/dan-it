import React from "react";
import ProductsList from "../ProductsList/ProductsList";
import "./checkoutdetails.scss";
import { useEffect } from "react";
export default function CheckoutDetails({ checkoutCart, userInfo }) {
  const { name, surname, age, address, phoneNumber } = userInfo;

  useEffect(() => {
    window.scrollTo(0, 0);
  });
  return (
    <>
      <h2 className="checkout-header">
        Those are your checkout details and selected items
      </h2>
      <ul className="user-list">
        <li className="user-list-item">Name : {name}</li>
        <li className="user-list-item"> Surname : {surname}</li>
        <li className="user-list-item"> Age : {age}</li>
        <li className="user-list-item"> Address : {address}</li>
        <li className="user-list-item">Phone Number : {phoneNumber}</li>
      </ul>
      <ProductsList products={checkoutCart} />
    </>
  );
}
