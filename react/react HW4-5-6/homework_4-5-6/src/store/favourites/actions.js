export const addToFavourites = (product) => {
  return {
    type: "ADD_TO_FAVOURITES",
    payload: product,
  };
};

export const removeFromFavourites = (product) => {
  return {
    type: "REVOME_FROM_FAVOURITES",
    payload: product,
  };
};

export const setFavourites = (product) => {
  return {
    type: "SET_FAVOURITES",
    payload: product,
  };
};
