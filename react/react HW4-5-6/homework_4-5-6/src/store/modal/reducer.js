export const modalReducer = (
  state = { modalDisplay: false, product: {} },
  action
) => {
  switch (action.type) {
    case "OPEN_MODAL":
      return { modalDisplay: true, product: action.payload };
    case "CLOSE_MODAL":
      return { modalDisplay: false, product: action.payload };

    default:
      return state;
  }
};
