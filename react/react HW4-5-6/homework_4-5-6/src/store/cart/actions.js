export const addToCart = (product) => {
  return {
    type: "ADD_TO_CART",
    payload: product,
  };
};

export const removeFromCart = (product) => {
  return {
    type: "REMOVE_FROM_CART",
    payload: product,
  };
};

export const setCart = (product) => {
  return {
    type: "SET_CART",
    payload: product,
  };
};

export const cleanCart = (product) => {
  return {
    type: "CLEAN_CART",
    payload: product,
  };
};
