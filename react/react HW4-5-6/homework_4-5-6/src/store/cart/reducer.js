export const cartReducer = (state = [], action) => {
  switch (action.type) {
    case "ADD_TO_CART":
      localStorage.setItem("cart", JSON.stringify([...state, action.payload]));
      return [...state, action.payload];
    case "REMOVE_FROM_CART":
      const filteredCart = state.filter((e) => e.name !== action.payload.name);
      localStorage.setItem("cart", JSON.stringify(filteredCart));
      return filteredCart;
    case "SET_CART":
      const cart = localStorage.getItem("cart");
      if (!cart) return [];
      const parsedCart = JSON.parse(cart);
      return parsedCart;
    case "CLEAN_CART":
      console.log(...state);

      localStorage.removeItem("cart");
      return [];
    default:
      return state;
  }
};
