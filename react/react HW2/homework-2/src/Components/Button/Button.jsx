import React from "react";
import "./button.scss";
import PropTypes from "prop-types";
const Button = ({ text, styleClass, openModal }) => {
  return (
    <button className={[styleClass, "button"].join(" ")} onClick={openModal}>
      {text}
    </button>
  );
};
Button.propTypes = {
  text: PropTypes.string,
  styleClass: PropTypes.string,
  openModal: PropTypes.func,
};
export default Button;
