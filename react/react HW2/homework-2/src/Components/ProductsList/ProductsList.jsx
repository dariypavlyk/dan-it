import Product from "../Product/Product";
import "./ProductsList.scss";
import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
const ProductsList = ({ products, addToCart, addFavourite }) => {
  return (
    <div className="products-container">
      <h2 className="phones-header">Phones list</h2>
      <ul className="phones-list">
        {products.map((product) => {
          return (
            <li className="phones-item" key={product.id}>
              <Product
                addToCart={addToCart}
                addFavourite={addFavourite}
                product={product}
              />
            </li>
          );
        })}
      </ul>
    </div>
  );
};
ProductsList.propTypes = {
  products: PropTypes.array,
};
export default ProductsList;
