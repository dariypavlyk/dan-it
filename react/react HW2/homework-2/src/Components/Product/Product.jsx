import "./Product.scss";
import Modal from "../Modal/Modal.jsx";
import Button from "../Button/Button.jsx";
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";

const Product = ({ product, addToCart, addFavourite }) => {
  const [favouriteDisplay, setfavouriteDisplay] = useState(false);
  const [modalDisplay, setModalDisplay] = useState(false);
  const showModal = (e) => {
    document.addEventListener("click", handleOutsideClick);
    setModalDisplay(true);
  };
  const closeModal = (e) => {
    setModalDisplay(false);
  };
  const handleOutsideClick = (e) => {
    if (
      !e.target.closest(".modal-content") &&
      !e.target.classList.contains("action-buy")
    ) {
      closeModal();
    }
  };
  const handleFavouriteDisplay = () => {
    setfavouriteDisplay(!favouriteDisplay);
  };

  const modalActions = (
    <div className="modal-footer">
      <button
        onClick={() => {
          addToCart(product.name);
          closeModal();
        }}
        className="btn btn-outline-primary"
      >
        Confirm
      </button>
      <button className="btn btn-outline-secondary">Cancel</button>
    </div>
  );
  const modalText = (
    <p className="modal-body">Would you like to add this phone to your cart?</p>
  );
  return (
    <>
      <div className="phone-image-container">
        <img src={product.url} alt="" className="phone-image" />
      </div>

      <h3 className="phone-name">{product.name}</h3>
      <div className="phone-details-container">
        <p>Color: {product.color}</p>

        <p>Product ID: {product.id}</p>
      </div>

      <p className="phone-price">{product.price} UAH</p>
      <div className="actions-container">
        <Button
          styleClass="action-buy"
          text="Add to cart"
          openModal={showModal}
        ></Button>

        <svg
          onClick={() => {
            addFavourite(product.name);
            handleFavouriteDisplay();
          }}
          width="16"
          height="16"
          fill={favouriteDisplay ? "#da2533" : "#797979"}
          className="phone-favicon"
        >
          <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
        </svg>
      </div>
      <Modal
        show={modalDisplay}
        onClose={closeModal}
        header={`${product.name}`}
        closeButton={true}
        text={modalText}
        actions={modalActions}
      />
    </>
  );
};
Product.propTypes = {
  product: PropTypes.object,
};
export default Product;
