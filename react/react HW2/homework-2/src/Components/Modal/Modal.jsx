import React from "react";
import "./modal.scss";
import PropTypes from "prop-types";
const Modal = ({ show, onClose, header, closeButton, text, actions }) => {
  const modalContent = (
    <div className="show modal-customized">
      <div className="modal-dialog ">
        <div className="modal-content ">
          <div className="modal-header">
            <h4 className="modal-title">{header}</h4>
            {closeButton ? (
              <button
                onClick={onClose}
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            ) : null}
          </div>
          {text}

          {actions}
        </div>
      </div>
    </div>
  );
  return !show ? null : modalContent;
};
Modal.propTypes = {
  show: PropTypes.bool,
  onClose: PropTypes.func,
  header: PropTypes.string,
  closeButton: PropTypes.bool,
  text: PropTypes.object,
  actions: PropTypes.object,
};
export default Modal;
