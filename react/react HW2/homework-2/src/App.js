import "./reset.css";
import "./App.scss";
import React, { useEffect, useState } from "react";
import { getProducts } from "./API/products";
import ProductsList from "./Components/ProductsList/ProductsList";
const App = () => {
  const [products, setProducts] = useState([]);
  const [cart, setCart] = useState([]);
  const [favourite, setFavourites] = useState([]);
  const addToCart = (name) => {
    setCart([...cart, name]);
  };
  const addFavourite = (name) => {
    setFavourites([...favourite, name]);
    console.log(favourite);
  };
  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cart));
  }, [cart]);
  useEffect(() => {
    localStorage.setItem("favourite", JSON.stringify(favourite));
  }, [favourite]);
  const fetchProducts = () => {
    getProducts().then((productsData) => {
      setProducts(productsData);
    });
  };
  useEffect(() => {
    fetchProducts();
  }, []);

  return (
    <>
      <ProductsList
        addToCart={addToCart}
        addFavourite={addFavourite}
        products={products}
      />
    </>
  );
};

export default App;
