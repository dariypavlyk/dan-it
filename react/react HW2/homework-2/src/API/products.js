export const getProducts = () =>
  fetch("/products.json").then((response) => response.json());
