let button = document.getElementsByClassName("btn");

for (let i = 0; i < button.length; i++) {
  button[i].dataset.button = button[i].innerHTML;
}

function activeButton(event) {
  for (let i = 0; i < button.length; i++) {
    button[i].style.backgroundColor = "#000000";
  }

  for (let i = 0; i < button.length; i++) {
    if (event.key === button[i].dataset.button) {
      button[i].style.backgroundColor = "#0000FF";
      console.log(button[i]);
    }
  }
}

document.body.addEventListener("keydown", activeButton);
