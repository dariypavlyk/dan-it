"use strict";

function filterBy(arr, dataType) {
  let result;

  result = arr.filter(function (element) {
    return typeof element !== dataType;
  });
  return result;
}

console.log(
  filterBy(
    [
      "hello",
      23,
      "23",
      true,
      null,
      [],
      {},
      undefined,
      1234567890123456789012345678901234567890n,
    ],
    "object"
  )
);
