// Creating the functions that will change the theme

function mainTheme(params) {
  let leftMenu = document.querySelectorAll(".left-menu-link");
  Array.from(leftMenu).forEach((el) => {
    el.style.color = "#495863";
  });
  let navbar = document.querySelectorAll(".navbar-list-link");
  Array.from(navbar).forEach((el) => {
    el.style.color = "#ffffff";
  });
  let imageBackground = document.querySelector(".image-container");
  imageBackground.style.backgroundColor = "#ffffff";
}

function secondTheme(params) {
  let leftMenu = document.querySelectorAll(".left-menu-link");
  Array.from(leftMenu).forEach((el) => {
    el.style.color = "blue";
  });
  let navbar = document.querySelectorAll(".navbar-list-link");
  Array.from(navbar).forEach((el) => {
    el.style.color = "green";
  });
  let imageBackground = document.querySelector(".image-container");
  imageBackground.style.backgroundColor = "red";
}

// Checking the current local storage value to set up the page theme

function colorCheck(params) {
  if (localStorage.getItem("bgcolour") === "1") {
    secondTheme();
  } else {
    mainTheme();
  }
}
colorCheck();

// Setting the functions on the button

let button = document.getElementById("change");

function changeColor(params) {
  if (localStorage.getItem("bgcolour") === "1") {
    localStorage.setItem("bgcolour", 0);
    mainTheme();
  } else {
    localStorage.setItem("bgcolour", 1);
    secondTheme();
  }
}

button.addEventListener("click", changeColor);
