"use strict";

let userName = prompt("Enter your name");
while (!userName || userName.trim() === "" || !isNaN(userName)) {
  userName = prompt("Enter your name");
}

let userAge = prompt("Enter your age");
while (!userAge || isNaN(userAge) || userAge.trim() === "") {
  userAge = prompt("Enter your age");
}

if (+userAge < 18) {
  alert(`You are not allowed to visit this website`);
} else if (+userAge <= 22) {
  let confirmation = confirm(`Are you sure you want to continue?`);
  if (confirmation) {
    alert(`Welcome, ${userName} `);
  } else {
    alert(`You are not allowed to visit this website`);
  }
} else {
  alert(`Welcome, ${userName} `);
}
