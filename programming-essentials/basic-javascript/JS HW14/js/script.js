// SETTING SMOOTH SCROLL TO THE PAGE

$(document).ready(function () {
  $("a").on("click", function (event) {
    if (this.hash !== "") {
      event.preventDefault();
      let hash = this.hash;
      $("html, body").animate(
        {
          scrollTop: $(hash).offset().top,
        },
        1000,
        function () {
          window.location.hash = hash;
        }
      );
    }
  });
});

// SETTING THE SCROLL-UP BUTTON

$(".btn").click(function () {
  $("html, body").animate(
    {
      scrollTop: 0,
    },
    1000
  );
});

$(window).scroll(function () {
  if ($(document).scrollTop() > $(window).height()) {
    $(".btn").show();
  } else {
    $(".btn").hide();
  }
});

// SECTION TOGGLE

let button = document.createElement("button");
let posts = document.getElementById("posts");
let div = document.createElement("div");
button.innerText = "Toggle";
button.classList.add("toggle-button");
div.style.display = "flex";
div.style.justifyContent = "center";
div.append(button);
posts.append(div);

$(".toggle-button").click(function () {
  $(".posts-container").slideToggle("slow", function () {});
});
