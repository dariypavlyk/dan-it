const createArrayList = function (arr, parent = "document.body") {
  const ul = document.createElement("ul");
  if (parent === "document.body") {
    document.body.append(ul);
  } else {
    let element = document.getElementById(`${parent}`);
    element.append(ul);
  }

  let newArr = arr.map((elem) => `<li>${elem}</li>`);
  ul.insertAdjacentHTML("beforeend", newArr.join(""));
};

console.log(createArrayList(["1", "2", "3", "sea", "user", 23], "div"));
