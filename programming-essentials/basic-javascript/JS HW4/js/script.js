function createNewUser() {
  let nameInput = prompt("Enter your first name");
  let lastNameInput = prompt("Enter your last name");

  let newUser = {
    firstName: nameInput,
    lastName: lastNameInput,
    getLogin: function () {
      return this.firstName.toLowerCase()[0] + this.lastName.toLowerCase();
    },
  };

  return newUser;
}

let user = createNewUser();
console.log(user.getLogin());
