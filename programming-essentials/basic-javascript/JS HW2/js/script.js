"use strict";
const coef = 5;

let n = prompt("Enter your number");

while (!n || isNaN(n) || n.trim() === "" || !Number.isInteger(+n) || n < 5) {
  alert("Sorry, no numbers");
  n = prompt("Enter your number");
}

for (let i = 0; i <= n; i = i + 5) {
  if (i % coef === 0) {
    console.log(i);
  }
}
