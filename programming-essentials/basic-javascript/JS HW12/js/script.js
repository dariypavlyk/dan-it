let pauseButton = document.getElementById("pause");
let playButton = document.getElementById("play");
let myIndex = 0;
let images = document.getElementsByClassName("image-to-show");
let slideCheck = false;

for (i = 0; i < images.length; i++) {
  images[i].style.display = "none";
}
images[3].style.display = "block";

function carousel() {
  for (let i = 0; i < images.length; i++) {
    images[i].style.display = "none";
  }
  myIndex++;
  if (myIndex > images.length) {
    myIndex = 1;
  }
  images[myIndex - 1].style.display = "block";
}

function pauseSlideshow() {
  slideCheck = false;
  clearInterval(slideInterval);
}

function playSlideshow() {
  if (slideCheck === false) {
    slideInterval = setInterval(carousel, 3000);
  }
  slideCheck = true;
}

pauseButton.addEventListener("click", pauseSlideshow);
playButton.addEventListener("click", playSlideshow);

playSlideshow();
