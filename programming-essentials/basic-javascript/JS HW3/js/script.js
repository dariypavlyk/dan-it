"use strict";

let num1 = prompt("Enter your first numbrer");
while (!num1 || isNaN(num1) || num1.trim() === "" || !Number.isInteger(+num1)) {
  num1 = prompt("Enter your first numbrer");
}

let num2 = prompt("Enter your second numbrer");
while (!num2 || isNaN(num2) || num2.trim() === "" || !Number.isInteger(+num2)) {
  num2 = prompt("Enter your second numbrer");
}

let operation = prompt("Enter the operation that you want to execute");
while (
  operation !== "+" &&
  operation !== "-" &&
  operation !== "/" &&
  operation !== "*"
) {
  operation = prompt("Enter the operation that you want to execute");
}

function calc(n1, n2, op) {
  switch (op) {
    case "+":
      let sum = +n1 + +n2;
      return sum;

    case "-":
      let rest = +n1 - +n2;
      return rest;

    case "*":
      let multiply = +n1 * +n2;
      return multiply;

    case "/":
      let divide = +n1 / +n2;
      return divide;
  }
}

calc(num1, num2, operation);
console.log(calc(num1, num2, operation));
