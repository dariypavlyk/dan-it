// declaration of variables

let pass = document.querySelectorAll("input");
let hideIcon = document.getElementsByClassName("fa-eye");
let displayerIcon = document.getElementsByClassName("fa-eye-slash");

// hiding the display icones

for (let i = 0; i < displayerIcon.length; i++) {
  displayerIcon[i].style.display = "none";
}
// setting functions to  display the password
function passDisplay0() {
  pass[0].type = "text";
  hideIcon[0].style.display = "none";
  displayerIcon[0].style.display = "block";
}
hideIcon[0].addEventListener("click", passDisplay0);

function passDisplay1() {
  pass[1].type = "text";
  hideIcon[1].style.display = "none";
  displayerIcon[1].style.display = "block";
}
hideIcon[1].addEventListener("click", passDisplay1);

// setting functions to hide the password
function passHide0() {
  pass[0].type = "password";
  hideIcon[0].style.display = "block";
  displayerIcon[0].style.display = "none";
}

displayerIcon[0].addEventListener("click", passHide0);

function passHide1() {
  pass[1].type = "password";
  hideIcon[1].style.display = "block";
  displayerIcon[1].style.display = "none";
}

displayerIcon[1].addEventListener("click", passHide1);

// comparing the password values and canceling the submit button

let submitButton = document.querySelector("form.password-form > button");
let labels = document.getElementsByClassName("input-wrapper");

const passWarning = document.createElement("span");
passWarning.innerText = "You need to enter the same value in both fields";
passWarning.style.color = "#FF0000";
passWarning.style.paddingBottom = "10px";
passWarning.style.display = "none";
labels[1].append(passWarning);

function passwordComparing(event) {
  if (pass[0].value === pass[1].value) {
    passWarning.style.display = "none";
    alert("You are welcome");
  } else if (pass[0].value !== pass[1].value) {
    passWarning.style.display = "block";
  }
  event.preventDefault();
}

submitButton.addEventListener("click", passwordComparing);
