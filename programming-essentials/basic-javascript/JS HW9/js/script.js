// declaring the variables

let tab = document.querySelector(".tabs");
let tabsContent = document.querySelectorAll("ul.tabs-content > li");
let tabs = document.querySelectorAll("ul.tabs > li");

// setting up the document

function nameSetUp(i) {
  tabs[i].dataset.name = tabs[i].innerHTML;
  tabsContent[i].dataset.name = tabs[i].innerHTML;
}
function displaySetUp(i) {
  for (let i = 1; i < tabsContent.length; i++) {
    tabsContent[i].style.display = "none";
  }
}

function documentSetUp() {
  for (let i = 0; i < tabs.length; i++) {
    nameSetUp(i);
    displaySetUp(i);
  }
}

window.addEventListener("DOMContentLoaded", documentSetUp);

// setting up the tab switch functionality

function tabSwitch(event) {
  if (event.target === tab) {
    return;
  }
  for (let i = 0; i < tabsContent.length; i++) {
    tabsContent[i].style.display = "none";
  }

  let active = document.querySelector(".active");
  active.classList.remove("active");

  for (let i = 0; i < tabsContent.length; i++) {
    if (event.target.dataset.name === tabsContent[i].dataset.name) {
      tabsContent[i].style.display = "block";
    }

    event.target.classList.add("active");
  }
  console.log(event.target);
}

tab.addEventListener("click", tabSwitch);
