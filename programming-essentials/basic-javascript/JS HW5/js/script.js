function createNewUser() {
  let nameInput = prompt("Enter your first name");
  let lastNameInput = prompt("Enter your last name");
  let birthdayInput = prompt("Enter your birthday like this  mm.dd.yyyy");

  let birthdayConversion = new Date(
    birthdayInput.substring(6, 10),
    birthdayInput.substring(0, 2) - 1,
    birthdayInput.substring(3, 5)
  );

  let newUser = {
    firstName: nameInput,
    lastName: lastNameInput,
    birthday: birthdayConversion,
    getLogin: function () {
      return this.firstName.toLowerCase()[0] + this.lastName.toLowerCase();
    },
    getAge: function () {
      let age = new Date() - this.birthday;
      return Math.floor(age / 31536000000);
    },
    getPassword: function () {
      return (
        this.firstName.toUpperCase()[0] +
        this.lastName.toLowerCase() +
        this.birthday.getFullYear()
      );
    },
  };

  return newUser;
}

let user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());
