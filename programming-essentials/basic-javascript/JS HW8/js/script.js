let price = document.getElementById("price");
price.type = "number";
function greenFocus() {
  price.style.borderColor = "#32CD32";
  price.style.borderWidth = "6px";
}

function whiteFocusOut() {
  price.style.borderColor = "";
  price.style.borderWidth = "";
}
price.addEventListener("focus", greenFocus);
price.addEventListener("focusout", whiteFocusOut);

let valueSection = document.createElement("div");
let valueSpan = document.createElement("span");
let closeButton = document.createElement("button");
valueSection.append(valueSpan);
valueSection.append(closeButton);
document.body.append(valueSection);
closeButton.style.display = "none";

function buttonCloser() {
  valueSection.style.display = "none";
  price.value = "";
  price.style.borderColor = "";
  price.style.borderWidth = "";
  price.style.color = "";
}

function valueOnFocusOut() {
  if (price.value === "") {
    valueSection.style.display = "none";
  } else if (+price.value < 0) {
    valueSection.style.display = "block";
    price.style.borderColor = "#FF6347";
    price.style.color = "";
    closeButton.style.display = "none";
    price.style.borderWidth = "6px";
    valueSpan.innerText = "Please enter the correct price";
  } else if (+price.value >= 0) {
    valueSection.style.display = "block";
    closeButton.style.display = "inline";
    valueSpan.innerText = `Текущая цена: ${price.value}`;
    closeButton.innerText = "X";
    closeButton.style.paddingRight = "2px";
    closeButton.style.paddingLeft = "2px";
    closeButton.style.marginLeft = "15px";
    closeButton.style.backgroundColor = "	#696969";
    price.style.color = "#32CD32";
    price.style.borderColor = "#32CD32";
    price.style.borderWidth = "6px";
    closeButton.addEventListener("click", buttonCloser);
  }
}

price.addEventListener("focusout", valueOnFocusOut);
