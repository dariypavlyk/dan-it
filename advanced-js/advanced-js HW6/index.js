const button = document.querySelector(".button");
const response = document.querySelector(".response");

button.addEventListener("click", async (event) => {
  const responseIp = await fetch(`https://api.ipify.org/?format=json`);
  const ipData = await responseIp.json();
  const { ip } = ipData;
  const responseLocation = await fetch(
    `http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district,zip,lat,lon`
  );
  const location = await responseLocation.json();
  const { continent, country, regionName, city, district } = location;
  response.textContent = `Continent : ${continent}, country : ${country}, Region : ${regionName}, City : ${city} ,District : ${district}`;
  console.log(response);

  return response;
});
