const request = fetch("https://ajax.test-danit.com/api/swapi/films");
const postsEl = document.createElement("div");
document.body.append(postsEl);

request
  .then((response) => {
    return response.json();
  })

  .then((response) => {
    const list = response.map((movie) => {
      const div = document.createElement("div");
      const h2 = document.createElement("h2");
      const p = document.createElement("p");
      const c = document.createElement("p");
      h2.textContent = movie.name;
      h2.textContent += " - Episode " + movie.episodeId;
      p.textContent = movie.openingCrawl;

      movie.characters.map((character) => {
        fetch(character)
          .then((link) => {
            return link.json();
          })
          .then((link) => {
            let {
              name = "Unknown",
              gender = "Unknown",
              height = "Unknown",
              birthYear = "Unknown",
              mass = "Unknown",
            } = link;
            c.innerText += `Character : ${name}, Gender :  ${gender}, Date of birth : ${birthYear}, Weight : ${mass}, Height : ${height}. \n `;
          });
      });
      div.append(h2, p, c);
      return div;
    });

    postsEl.append(...list);
  });
