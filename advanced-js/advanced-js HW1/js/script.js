class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  get name() {
    return this._name;
  }
  set name(name) {
    this._name = name;
  }
  get age() {
    return this._age;
  }
  set age(age) {
    this._age = age;
  }
  get salary() {
    return this._salary;
  }
  set salary(salary) {
    this._salary = salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  get salary() {
    return this._salary * 3;
  }
  set salary(salary) {
    this._salary = salary;
  }
}
const employee = new Employee("John", "Doe", 1500);
const programmer1 = new Programmer("Peter", "Parker", 2400, "go");
const programmer2 = new Programmer("Phil", "Dumphy", 5000, "scala");
const programmer3 = new Programmer("Peter", "Parker", 3500, "kotlin");

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);
console.log(programmer1.salary);
console.log(programmer2.salary);
console.log(programmer3.salary);
