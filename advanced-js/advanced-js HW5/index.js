class Element {
  createElement(elemType, classNames = [], text) {
    const element = document.createElement(elemType);
    if (text) {
      element.textContent = text;
    }
    element.classList.add(...classNames);
    return element;
  }
}

class Card extends Element {
  constructor() {
    super();
  }
  renderInfo() {
    const userCard = this.createElement("div", ["card"]);
    const info = this.createElement("ul", ["card_info"]);
    const username = this.createElement(
      "li",
      ["card_info-item"],
      `${this.name}`
    );
    const usermail = this.createElement(
      "li",
      ["card_info-item"],
      `${this.email}`
    );
    info.append(username, usermail);
    userCard.append(info);
    return userCard;
  }
}

class userInfo extends Card {
  constructor(name, email, id) {
    super();
    this.name = name;
    this.email = email;
    this.id = id;
  }
}
class userText extends Card {
  constructor(title, body, userId) {
    super();
    this.title = title;
    this.body = body;
    this.userId = userId;
  }
  renderText() {
    const text = this.createElement("div", ["card_text"]);
    const textTitle = this.createElement(
      "h3",
      ["card_text_heading"],
      `${this.title}`
    );
    const textBody = this.createElement(
      "p",
      ["card_text_body"],
      `${this.body}`
    );
    // const closeButton = document.createElement("button");
    // closeButton.innerText = "Close";
    // closeButton.classList.add("close_button");
    text.append(textTitle, textBody);
    return text;
  }
}
// const deleteTodo = (id) =>
//   fetch(`https://ajax.test-danit.com/api/json/users/${id}`, {
//     method: "DELETE",
//   });

async function displayCards() {
  const usersResponse = await fetch(
    `https://ajax.test-danit.com/api/json/users`
  );
  const postsResponse = await fetch(
    `https://ajax.test-danit.com/api/json/posts`
  );
  const users = await usersResponse.json();
  const posts = await postsResponse.json();
  console.log(users);
  console.log(posts);
  const root = document.querySelector(".root");
  users.forEach(async (user) => {
    const { name, email, id } = user;
    const userInf = new userInfo(name, email, id);

    posts.forEach(async (post) => {
      const info = userInf.renderInfo();
      const { title, body, userId } = post;
      const userPost = new userText(title, body, userId);
      if (id === userId) {
        info.append(userPost.renderText());
        const closeButton = document.createElement("button");
        closeButton.innerText = "Close";
        closeButton.classList.add("close_button");
        closeButton.addEventListener("click", function remove() {
          info.remove(),
            fetch(`https://ajax.test-danit.com/api/json/users/${id}`, {
              method: "DELETE",
            });
        });
        info.append(closeButton);
        root.append(info);
      }
    });
  });
}

displayCards();
